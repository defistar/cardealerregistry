kanth@kanth-MacBook-Pro carbooking % truffle migrate --reset --network ropsten

Compiling your contracts...
===========================
> Compiling ./contracts/CarDealerRegistry.sol
> Compiling ./contracts/Migrations.sol
> Compilation warnings encountered:

    /Users/kanth/Documents/dao/carbooking/contracts/CarDealerRegistry.sol:2:1: Warning: Experimental features are turned on. Do not use experimental features on live deployments.
pragma experimental ABIEncoderV2;
^-------------------------------^

> Artifacts written to /Users/kanth/Documents/dao/carbooking/build/contracts
> Compiled successfully using:
   - solc: 0.5.16+commit.9c3226ce.Emscripten.clang



Starting migrations...
======================
> Network name:    'ropsten'
> Network id:      3
> Block gas limit: 8000000 (0x7a1200)


1_initial_migration.js
======================
1_initial_migration:  ropsten

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0x0ccfa8865be348f0b845ebdad0ad646c64e3b9c5294b05ffd938b002fb22beb3
   > Blocks: 3            Seconds: 13
   > contract address:    0x188a0dCDD73Fd1a16bb2e48751F98916e90c2d92
   > block number:        9931776
   > block timestamp:     1617009042
   > account:             0x1F61741610892C0A4D3e831Dc91958df2dc27182
   > balance:             0.93150724
   > gas used:            226537 (0x374e9)
   > gas price:           30 gwei
   > value sent:          0 ETH
   > total cost:          0.00679611 ETH

   Pausing for 1 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 9931777)

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00679611 ETH


2_deploy_contracts.js
=====================
default account: 0x1F61741610892C0A4D3e831Dc91958df2dc27182

   Deploying 'CarDealerRegistry'
   -----------------------------
   > transaction hash:    0x974a5e0c77c8a0fef4f56380be2d48a881f1bc053758aa20b2b02176ce24358e
   > Blocks: 2            Seconds: 9
   > contract address:    0x64acdac221E0BabdB1e3704c097c0ABaADddfFA5
   > block number:        9931784
   > block timestamp:     1617009103
   > account:             0x1F61741610892C0A4D3e831Dc91958df2dc27182
   > balance:             0.89249404
   > gas used:            1254677 (0x132515)
   > gas price:           30 gwei
   > value sent:          0 ETH
   > total cost:          0.03764031 ETH

   Pausing for 1 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 9931785)
carDealerRegistryDeployedInstance is deployed with Address: 
    0x64acdac221E0BabdB1e3704c097c0ABaADddfFA5

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.03764031 ETH


Summary
=======
> Total deployments:   2
> Final cost:          0.04443642 ETH


kanth@kanth-MacBook-Pro carbooking % 

    truffle version

Truffle v5.2.4 (core: 5.2.4)
Solidity v0.5.16 (solc-js)
Node v10.22.1
Web3.js v1.2.9