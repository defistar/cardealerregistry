const { default: Web3 } = require("web3");

const CarDealerRegistry = artifacts.require('./CarDealerRegistry.sol');

module.exports = function (deployer, network, accounts) {
  deployer.then(async () => {
    console.log(`default account: ${accounts[0]}`);
    //await deployer.deploy(CarDealerRegistryStructLib);
    //deployer.link(CarDealerRegistryStructLib, CarDealerRegistry);
    const carDealerRegistryDeployedInstance = await deployer.deploy(CarDealerRegistry,accounts[0]);
    console.log(`carDealerRegistryDeployedInstance is deployed with Address: 
    ${carDealerRegistryDeployedInstance.address}`);
  });
};
