
# Solidity Q&A:


## Event and log record

- What's the event and it used for?

 1. events are used to debug , detect specific state changes
 2. notify of the logs that something happened
 3. Events/Logs are useful when writing or interacting with smart contracts
 4. Confirmation or warning messages in a function.
 5. event logs are useful to let Dapps/Apps Integrated, know something has happened without having to query contracts individually.


- What parts does the log record have to store?

 1. Log Record consists of topics and data
    
    - Topics are 32 Byte (256 bit) "words" that are used to describe what's going on in the event.
    - Opcodes LOG0, LOG1, LOG2, LOG3, LOG4, LOG5 represent the logs
    - Number in the Opcode represent the number of topics in the Log/Event
    - A log can contain a maximum of 4 topics. (in a Single Log record)
 
 2. Anatomy of Log record:

      - topics and indexed-topics:  
        
        - Topics are searchable (indexed topics), where-as data is not searchable
      
    - Log record contain 2 parts

      - part-1:
        
         - 1st part contains keccak256 hash of the signature of the event (name, datatypes in the event)
         - only exception is the anonymous-events 

      - part-2:   
        
        - 2nd part contains additional-data. 
        
      - topics vs data:  
         
         - Including data is a lot cheaper than including indexed-topics
         - topics are limited to 4 (4 * 32 Bytes), which means it cannot include large complicated data
         - eventually it's on rocksdb where all state is stored (which is scalable to run on servers with many CPU cores)
         - event-data can be used to include the value


## Where are the variables of different types stored    
   
    - Variables of different types are stored in storage-trie
    - Every account has its own storage-trie
    - Every Smart contract has its own storage which is eventually stored in storage-trie.
    - A 256 bit hash of the storage-trie's root-node is a storageRoot value in the global-state-trie
    - Variables of events are stored in transaction-receipt-trie


## Call and DelegateCal

### What are Call and DelegateCall?

   - call is transfer of execution context to the contract which was called.

   - delegatecall is delegating the execution and but it runs in the context of the one which has delegated
      - msg,sender is retained even after the call is delegated
   - delegatecall is fix to opcode callcode, which didnot preserve msg.sender
   - delegatecall is calling mechanism of how caller contract calls target contract function but when target contract executes logic, context is 
   

### What’s the relationship between them?

   - delegatecall is advanced form of call, main difference is that delegatecall maintains the context where as call opcode changes the context


## What’s the Reentrancy attack/recursive call?

   - re-entrancy attack is attack on a vulnerable function by the clients who are aware of the vulnerability
   - vuilerability is about the ability for external contract/client taking control of funds in contract or exhaust funds while making transaction
   - re-entrancy happens mostly by making recursive callback to a original vulnerable contract function
   - renentrancy attacks mostly involve send/transfer/call functions. 

### howto avoid Reentrancy attack?

  1. send vs transfer vs call
     - send and transfer are safer because they limit the gas to 2300, gas-limit prevents the expensive external function calls back to the target vulnerable contract

  2. security libraries to guard from reentrancy / mutex locks
   - use re-entrancy guard to prevent reentering or recursive calls, we can use openzeppelin contracts for re-entrancy guard instead of building a new one
   - placing a mutex, which is a lock on the contract state (to prevent cross-function re-entrancy attacks)

  3. tx.origin vs msg.sender

   - There are two variables – tx.origin and msg.sender – in Solidity contract’s global namespace which look very similar, 
     but interchanging them might lead to a severe security vulnerability. 

   - Tx.origin returns address which initiated the current transaction. 
   - On the other hand msg.sender returns address which originated current message call.  

   - always use msg.sender in the checks in your function,
   - if tx.origin is used to check, abuser will write a fallback function and make the vulnerable function to call your fallback which essentially transfers all its funds to attacker.


## relationship and difference between Bytesand String

- What’s the relationship and difference between Bytes and String? 

    bytes are arbitrary-length raw byte data 
    string is arbitrary-length string (UTF-8) data.


- What are they usedfor respectively?

    If you can limit the length to a certain number of bytes, always use one of bytes1 to bytes32 because they are much cheaper.
    string is identical to bytes only that it is assumed to hold the UTF-8 encoding of a real string.
    Since string stores the data in UTF-8 encoding it is quite expensive to compute the number of characters in the string (the encoding of some characters takes more than a single byte). Because of that,


- When to use Bytes instead of String and String instead of Bytes?     

  - when you are aware that string's length wont exceed 32 bytes, use bytes32
    because it fits in a single word in the EVM, while string is a dynamic array which consumes more gas for initial storage allocation and further expansions.
  
  - use String if it has varying length




