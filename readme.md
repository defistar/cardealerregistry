# CarDealerRegistry:

## Features:

1. add a car to invetory
2. Make a booking or record sale of a car

## Data-Structure:

1. Car
2. CarBooking

## Deployed contract to ropsten:

- https://ropsten.etherscan.io/address/0x64acdac221e0babdb1e3704c097c0abaadddffa5

### Read Contract:

- https://ropsten.etherscan.io/address/0x64acdac221e0babdb1e3704c097c0abaadddffa5#readContract

### Write to Contract:

- https://ropsten.etherscan.io/address/0x64acdac221e0babdb1e3704c097c0abaadddffa5#writeContract


## Transactions:

Sample Transaction for add car to inventory:

https://ropsten.etherscan.io/tx/0x361812728bd0642cfd54727fa44d30307bc1c1d7b084726a68b7de6655462c1d

